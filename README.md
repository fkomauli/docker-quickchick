Docker image with Coq 8.4 and QuickChick installed from sources.

## Environment

* Coq 8.4
* ssreflect 1.5 (required by QuickChick)
* QuickChick (version: itp-2015-final)
* Proof General

## Execution

Inside your Coq project's root folder, run:

```
docker run --rm -it -v `pwd`:/coq fkomauli/quickchick
```

## Licenses

### Coq

GNU LESSER GENERAL PUBLIC LICENSE
Version 2.1, February 1999

### QuickChick

The MIT License (aka Expat License)

Copyright (c)  2014  Maxime Dénès, Catalin Hritcu,
Leonidas Lampropoulos, and Zoe Paraskevopoulou
